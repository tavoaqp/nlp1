package ime.nlp

import scala.collection.mutable._
import scala.collection.generic._

object EarleyParser {

  var isNextCatPOS = new Function2[GrammarElement, Grammar, Boolean]() {

    def apply(nextCat: GrammarElement, grammar: Grammar): Boolean = {
      if (grammar.contains(nextCat)) {
        for (elemList <- grammar(nextCat)) {
          var termCount = elemList.filter(t => t.isInstanceOf[Term]).size
          if (termCount > 0)
            return true
        }
      }

      return false
    }
  }

  //OhHaey4a
  var scanner = new Function4[State, ArrayBuffer[Tuple2[ArrayBuffer[State], HashSet[State]]], Grammar, ArrayBuffer[String], Unit]() {
    def apply(state: State, chart: ArrayBuffer[Tuple2[ArrayBuffer[State], HashSet[State]]], grammar: Grammar, words: ArrayBuffer[String]): Unit = {
      if (state.j < words.size) {
        var jWord = words(state.j)
        var posList = HashSet[GrammarElement]()
        if (grammar.wordCatInvList.get.contains(jWord))
          posList = grammar.wordCatInvList.get(jWord)
        var nextCat = state.nextCategory
        if (posList.contains(nextCat)) {
          var newState = State(None, nextCat, ElemList(Term(jWord)), 1, state.j, state.j + 1)
          if (!chart(state.j + 1)._2.contains(newState)) {
            chart(state.j + 1)._1 += newState
            chart(state.j + 1)._2 += newState
            // System.out.println(newState+" SCANNER")
          }
        }
      }

    }
  }

  var scannerLog = new Function4[State, ArrayBuffer[Tuple2[ArrayBuffer[State], HashSet[State]]], Grammar, ArrayBuffer[String], Unit]() {
    def apply(state: State, chart: ArrayBuffer[Tuple2[ArrayBuffer[State], HashSet[State]]], grammar: Grammar, words: ArrayBuffer[String]): Unit = {
      if (state.j < words.size) {
        var jWord = words(state.j)
        var posList = HashSet[GrammarElement]()
        if (grammar.wordCatInvList.get.contains(jWord))
          posList = grammar.wordCatInvList.get(jWord)
        var nextCat = state.nextCategory
        if (posList.contains(nextCat)) {
          var newState = State(None, nextCat, ElemList(Term(jWord)), 1, state.j, state.j + 1)
          if (!chart(state.j + 1)._2.contains(newState)) {
            chart(state.j + 1)._1 += newState
            chart(state.j + 1)._2 += newState
            System.out.println(newState + " SCANNER")
          }
        }
      }

    }
  }

  var deterministicPredictorLog = new Function3[State, ArrayBuffer[Tuple2[ArrayBuffer[State], HashSet[State]]], Grammar, Unit]() {

    def apply(state: State, chart: ArrayBuffer[Tuple2[ArrayBuffer[State], HashSet[State]]], grammar: Grammar): Unit = {
      var nextCat = state.nextCategory
      // System.out.println("nextCat="+nextCat)
      var elemIdx = 0
      while (elemIdx < grammar(nextCat).size) {
        var elemList = grammar(nextCat)(elemIdx)
        var newState = State(None, nextCat, elemList, 0, state.j, state.j)
        if (!chart(state.j)._2.contains(newState)) {
          chart(state.j)._1 += newState
          chart(state.j)._2 += newState
          System.out.println(newState + " PREDICTOR")
        }
        elemIdx += 1
      }
      // for (elemList <- grammar(nextCat)){

      // }
    }
  }

  var deterministicCompleterLog = new Function3[State, ArrayBuffer[Tuple2[ArrayBuffer[State], HashSet[State]]], HashMap[Int, State], Unit]() {

    def apply(state: State, chart: ArrayBuffer[Tuple2[ArrayBuffer[State], HashSet[State]]], ruleMap: HashMap[Int, State]): Unit = {

      var j = state.i
      var k = state.j
      var othIdx = 0
      while (othIdx < chart(j)._1.size) {
        var othState = chart(j)._1(othIdx)
        if (othState.nextCategory.equals(state.left)) {
          var nextState = othState.advanceMark
          nextState.j = k
          if (!chart(k)._2.contains(nextState)) {
            ruleMap += Tuple2(state.id.get, state)
            nextState.addChildren(state.id.get)
            chart(k)._1 += nextState
            chart(k)._2 += nextState
            System.out.println(nextState + " COMPLETER from " + state)
          }
        }
        othIdx += 1
      }

    }
  }

  var deterministicPredictor = new Function3[State, ArrayBuffer[Tuple2[ArrayBuffer[State], HashSet[State]]], Grammar, Unit]() {

    def apply(state: State, chart: ArrayBuffer[Tuple2[ArrayBuffer[State], HashSet[State]]], grammar: Grammar): Unit = {
      var nextCat = state.nextCategory
      // System.out.println("nextCat="+nextCat)
      var elemIdx = 0
      while (elemIdx < grammar(nextCat).size) {
        var elemList = grammar(nextCat)(elemIdx)
        var newState = State(None, nextCat, elemList, 0, state.j, state.j)
        if (!chart(state.j)._2.contains(newState)) {
          chart(state.j)._1 += newState
          chart(state.j)._2 += newState
          // System.out.println(newState+" PREDICTOR")
        }
        elemIdx += 1
      }
      // for (elemList <- grammar(nextCat)){

      // }
    }
  }

  var deterministicCompleter = new Function3[State, ArrayBuffer[Tuple2[ArrayBuffer[State], HashSet[State]]], HashMap[Int, State], Unit]() {

    def apply(state: State, chart: ArrayBuffer[Tuple2[ArrayBuffer[State], HashSet[State]]], ruleMap: HashMap[Int, State]): Unit = {

      var j = state.i
      var k = state.j
      var othIdx = 0
      while (othIdx < chart(j)._1.size) {
        var othState = chart(j)._1(othIdx)
        if (othState.nextCategory.equals(state.left)) {
          var nextState = othState.advanceMark
          nextState.j = k
          if (!chart(k)._2.contains(nextState)) {
            ruleMap += Tuple2(state.id.get, state)
            nextState.addChildren(state.id.get)
            chart(k)._1 += nextState
            chart(k)._2 += nextState
            // System.out.println(nextState+" COMPLETER from "+state)
          }
        }
        othIdx += 1
      }

    }
  }

  var probabilisticPredictor = new Function3[State, ArrayBuffer[Tuple2[ArrayBuffer[State], HashSet[State]]], Grammar, Unit]() {

    def apply(state: State, chart: ArrayBuffer[Tuple2[ArrayBuffer[State], HashSet[State]]], grammar: Grammar): Unit = {
      var nextCat = state.nextCategory
      // System.out.println("nextCat="+nextCat)
      var elemIdx = 0
      while (elemIdx < grammar(nextCat).size) {
        var elemList = grammar(nextCat)(elemIdx)
        var newState = State(None, nextCat, elemList, 0, state.j, state.j)
        newState.score = elemList.prob
        if (!chart(state.j)._2.contains(newState)) {
          chart(state.j)._1 += newState
          chart(state.j)._2 += newState
          // System.out.println(newState+" PREDICTOR")
        }
        elemIdx += 1
      }
      // for (elemList <- grammar(nextCat)){

      // }
    }
  }

  var probabilisticCompleter = new Function3[State, ArrayBuffer[Tuple2[ArrayBuffer[State], HashSet[State]]], HashMap[Int, State], Unit]() {

    def apply(state: State, chart: ArrayBuffer[Tuple2[ArrayBuffer[State], HashSet[State]]], ruleMap: HashMap[Int, State]): Unit = {

      var j = state.i
      var k = state.j
      var othIdx = 0
      while (othIdx < chart(j)._1.size) {

        var othState = chart(j)._1(othIdx)

        if (othState.nextCategory.equals(state.left)) {

          var nextState = othState.advanceMark
          nextState.j = k
          // System.out.println("nextState="+nextState)
          // System.out.println("state="+state)
          nextState.score = Some(nextState.score.get * state.score.get)
          if (!chart(k)._2.contains(nextState)) {
            ruleMap += Tuple2(state.id.get, state)
            nextState.addChildren(state.id.get)
            chart(k)._1 += nextState
            chart(k)._2 += nextState
            // System.out.println(nextState+" COMPLETER from "+state)
          }
        }
        othIdx += 1
      }

    }
  }

  var probabilisticScanner = new Function4[State, ArrayBuffer[Tuple2[ArrayBuffer[State], HashSet[State]]], Grammar, ArrayBuffer[String], Unit]() {

    def apply(state: State, chart: ArrayBuffer[Tuple2[ArrayBuffer[State], HashSet[State]]], grammar: Grammar, words: ArrayBuffer[String]): Unit = {
      if (state.j < words.size) {
        var jWord = words(state.j)
        var posList = HashSet[GrammarElement]()
        if (grammar.wordCatInvList.get.contains(jWord))
          posList = grammar.wordCatInvList.get(jWord)
        var nextCat = state.nextCategory
        if (posList.contains(nextCat)) {
          var term = Term(jWord)
          var newState = State(None, nextCat, ElemList(term), 1, state.j, state.j + 1)

          if (!chart(state.j + 1)._2.contains(newState)) {
            newState.score = grammar(nextCat).filter(t => t.contains(term))(0).prob
            chart(state.j + 1)._1 += newState
            chart(state.j + 1)._2 += newState
            // System.out.println(newState+" SCANNER")
          }
        }
      }

    }
  }

  var defaultTreeSelector = new Function2[ArrayBuffer[State], HashMap[Int, State], ArrayBuffer[Tree]]() {

    def apply(roots: ArrayBuffer[State], ruleMap: HashMap[Int, State]): ArrayBuffer[Tree] = {

      roots.map(t => TreeExtractor.buildTree(t, ruleMap))
    }
  }

  var maxProbTreeSelector = new Function2[ArrayBuffer[State], HashMap[Int, State], ArrayBuffer[Tree]]() {

    def apply(roots: ArrayBuffer[State], ruleMap: HashMap[Int, State]): ArrayBuffer[Tree] = {
      var maxRoot = roots.sortWith((a, b) => a.score.get > b.score.get)(0)
      ArrayBuffer[Tree]() += TreeExtractor.buildTree(maxRoot, ruleMap)

    }
  }
}

class EarleyParser {

  def parse(words: ArrayBuffer[String], grammar: Grammar,
    isNextCatPOS: (GrammarElement, Grammar) => Boolean,
    scanner: (State, ArrayBuffer[Tuple2[ArrayBuffer[State], HashSet[State]]], Grammar, ArrayBuffer[String]) => Unit,
    predictor: (State, ArrayBuffer[Tuple2[ArrayBuffer[State], HashSet[State]]], Grammar) => Unit,
    completer: (State, ArrayBuffer[Tuple2[ArrayBuffer[State], HashSet[State]]], HashMap[Int, State]) => Unit,
    treeSelector: (ArrayBuffer[State], HashMap[Int, State]) => ArrayBuffer[Tree]): ArrayBuffer[Tree] =
    {

      var ruleMap = HashMap[Int, State]()
      var chart = ArrayBuffer[Tuple2[ArrayBuffer[State], HashSet[State]]]()
      Range(0, words.size + 1).foreach {
        case idx => chart += Tuple2(ArrayBuffer[State](), HashSet[State]())
      }
      var dummyState = State(None, Var("Y"), ElemList(Var("START")), 0, 0, 0)
      chart(0)._1 += dummyState
      chart(0)._2 += dummyState

      var ruleIdx = 0
      var foundRoots = ArrayBuffer[State]()
      var wordIdx = 0
      while (wordIdx <= words.size) {
        // if (wordIdx < 2)
          // System.out.println("\nI=" + wordIdx)
        var idx = 0
        while (idx < chart(wordIdx)._1.size) {
          var state = chart(wordIdx)._1(idx)
          state.id = Some(ruleIdx)

          var isNextCatPOSVal = isNextCatPOS(state.nextCategory, grammar)
          var stateComplete = state.complete

          if (!stateComplete && !isNextCatPOSVal) {
            predictor(state, chart, grammar)
          } else if (!stateComplete && isNextCatPOSVal) {
            // System.out.println("Trying to scan at "+state)
            scanner(state, chart, grammar, words)
          } else {
            completer(state, chart, ruleMap)
          }

          idx += 1
          ruleIdx += 1
          state.left match {
            case varObj: Var => {
              if (varObj.id == "START" && state.complete && state.i == 0 && state.j == words.size) {
                // System.out.println("\nFound root="+state)
                foundRoots += state
              }
            }
            case _ =>
          }

        }
        if (wordIdx == 0) {
          chart(wordIdx)._1 -= dummyState
          chart(wordIdx)._2 -= dummyState
        }

        // if (wordIdx < 2)
        //   System.out.println("\nNextIteration Row I=" + (wordIdx) + " " + chart(wordIdx))

        wordIdx += 1

        // System.out.println(chart(wordIdx))
        
      }

      // System.out.println("\nruleMap="+ruleMap)
      foundRoots.map(t => TreeExtractor.buildTree(t, ruleMap))
    }

}
