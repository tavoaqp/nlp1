package ime.nlp

import scala.collection.mutable._
import scala.collection.generic._

object SentenceExtractor {

  def extractSentence(tree: Tree): ArrayBuffer[String] = {
    var sentence = ArrayBuffer[String]()
    var queue: Stack[Node] = Stack[Node]()
    queue push tree.root
    while (!queue.isEmpty) {
      var nextNode = queue.pop
      nextNode.id match {
        case Term(value) =>
          {
            value match {
              case "(" =>
              case ")" =>
              case other => {
                var splits = CorpusTraining.regexPunct.findAllIn(value)
                if (splits.hasNext) {
                  sentence += value
                }
              }

            }
          }

        case _ =>
      }
      if (!nextNode.children.isEmpty)
        nextNode.children.get.reverse.foreach(t => queue.push(t))

    }
    sentence
  }
}

object GrammarExtractor {

  def uniqueRules(grammar: Grammar) {
    grammar.keys.foreach(k => grammar(k) = grammar(k).distinct)
  }

  def cleanInconsistentRules(grammar: Grammar) {
    grammar.keys.foreach(k => {
      
      var varElems = grammar(k).filter(t => t.filter(u => u.isInstanceOf[Var]).size > 0).size

      var term=grammar(k).filter(t => t.filter(u => u.isInstanceOf[Term]).size > 0)

      var termElems = term.size

      if (termElems > 0 && varElems > 0) {
        if (termElems > varElems) {

          grammar(k) = grammar(k).filter(t => t.filter(u => u.isInstanceOf[Term]).size > 0)
        } else {
          term.flatMap(t=>t).foreach( t => grammar.wordCatInvList.foreach( d => { d(t.asInstanceOf[Term].id)-=k } ))

          grammar(k) = grammar(k).filter(t => t.filter(u => u.isInstanceOf[Var]).size > 0)

          // System.out.println("After correction="+grammar(k))
        }
      }

    })
  }

  def extract(trees: ArrayBuffer[Tree]): Grammar = {

    var grammar = Grammar()
    grammar.wordCatInvList = Some(HashMap[String, HashSet[GrammarElement]]())
    var allHeaders = ElemList()
    for (tree <- trees) {
      var headers = findRules(tree.root, grammar)
      allHeaders ++= headers

    }

    grammar += Tuple2(Var("START"), ElemListArray())

    for (header <- allHeaders.distinct) {
      grammar(Var("START")) += ElemList(header)
    }

    uniqueRules(grammar)
    cleanInconsistentRules(grammar)
    grammar
  }  

  def countProbabilitiesWithLaplace(grammar: Grammar) {

    var allRules=grammar.map ( t => t._2.map( u => (t._1,u)) ).flatMap(t => t)

    var counts=allRules.groupBy ( t => t).map ( t => (t._1,t._2.size))
    var countHeads=allRules.map( t => (t._1.asInstanceOf[Var],1)).groupBy( t => t._1).map (t => (t._1,t._2.size))

    var nonZeroHeadsCount=countHeads.filter (t => t._2>0).size.toDouble+1

    grammar.keys.foreach( k => {
    		var headProb=countHeads(k.asInstanceOf[Var]).toDouble
    		var idx=0
    		while (idx < grammar(k).size){
    			var varList=grammar(k)(idx)    			
    			var listProb=counts(Tuple2(k,varList))    			
    			varList.prob=Some((1+listProb.toDouble)/(headProb+nonZeroHeadsCount))
    			idx+=1
    		}
    	})
  }

  def countProbabilities(grammar: Grammar) {

    var allRules=grammar.map ( t => t._2.map( u => (t._1,u)) ).flatMap(t => t)

    var counts=allRules.groupBy ( t => t).map ( t => (t._1,t._2.size))
    var countHeads=allRules.map( t => (t._1.asInstanceOf[Var],1)).groupBy( t => t._1).map (t => (t._1,t._2.size))

    var nonZeroHeadsCount=countHeads.filter (t => t._2>0).size.toDouble+1

    grammar.keys.foreach( k => {
    		var headProb=countHeads(k.asInstanceOf[Var]).toDouble
    		var idx=0
    		while (idx < grammar(k).size){
    			var varList=grammar(k)(idx)    			
    			var listProb=counts(Tuple2(k,varList))    			
    			varList.prob=Some((listProb.toDouble)/(headProb))
    			idx+=1
    		}
    	})
  }

  def extractProbabilistic(trees: ArrayBuffer[Tree]): Grammar = {

    var grammar = Grammar()
    grammar.wordCatInvList = Some(HashMap[String, HashSet[GrammarElement]]())
    var allHeaders = ElemList()
    for (tree <- trees) {
      var headers = findRules(tree.root, grammar)
      allHeaders ++= headers

    }

    grammar += Tuple2(Var("START"), ElemListArray())

    var headersProb=allHeaders.size.toDouble
    for (header <- allHeaders.distinct) {
      var elemList=new ElemList(Some(1.0/headersProb))
      elemList+=header
      grammar(Var("START")) += elemList
    }

    countProbabilities(grammar)
    uniqueRules(grammar)
    cleanInconsistentRules(grammar)
    grammar
  }

  def extractProbabilisticWithLaplace(trees: ArrayBuffer[Tree]): Grammar = {

    var grammar = Grammar()
    grammar.wordCatInvList = Some(HashMap[String, HashSet[GrammarElement]]())
    var allHeaders = ElemList()
    for (tree <- trees) {
      var headers = findRules(tree.root, grammar)
      allHeaders ++= headers

    }

    grammar += Tuple2(Var("START"), ElemListArray())

    var headersProb=allHeaders.size.toDouble
    for (header <- allHeaders.distinct) {
      var elemList=new ElemList(Some(1.0/headersProb))
      elemList+=header
      grammar(Var("START")) += elemList
    }

    countProbabilitiesWithLaplace(grammar)
    uniqueRules(grammar)
    cleanInconsistentRules(grammar)
    grammar
  }

  def findRules(node: Node, grammar: Grammar): ElemList = {
    var headers = ElemList()
    var size = node.children.get.size
    var children = node.children.get
    // System.out.println("\nFinding rules for "+node)
    // System.out.println("Size "+size)
    // System.out.println("Children "+node.children)
    size match {
      case 1 => {
        // System.out.println("Node="+node+" Inspecting "+children(0).children.get(0))
        return ElemList(Term(children(0).children.get(0).id.asInstanceOf[Term].id))
      }

      case 4 => {
        var newVar = Var(children(1).children.get(0).id.asInstanceOf[Term].id)
        // System.out.println("Node="+node+" Found left="+newVar)
        headers += newVar
        var rightSide = findRules(children(2), grammar)
        if (!grammar.contains(newVar))
          grammar += Tuple2(newVar, ElemListArray())
        // System.out.println("Node="+node+" Size="+size+ " Found rightSide="+rightSide)
        grammar(newVar) += rightSide

        if (children(2).children.get.size == 1) {
          var term = rightSide(0).asInstanceOf[Term].id

          if (!grammar.wordCatInvList.get.contains(term))
            grammar.wordCatInvList.get += Tuple2(term, HashSet[GrammarElement]())

          if (!grammar.wordCatInvList.get(term).contains(newVar))
            grammar.wordCatInvList.get(term) += newVar

        }
        // System.out.println("Node="+node+" Size="+size+" Found headers="+headers)
        return headers
      }
      case _ => {
        var newVar = Var(children(1).children.get(0).id.asInstanceOf[Term].id)
        // System.out.println("Node="+node+" Found left="+newVar)
        headers += newVar
        var rightSide = findRules(children(2), grammar)

        if (!grammar.contains(newVar))
          grammar += Tuple2(newVar, ElemListArray())

        grammar(newVar) += rightSide

        if (children(2).children.get.size == 1) {
          var term = rightSide(0).asInstanceOf[Term].id

          if (!grammar.wordCatInvList.get.contains(term))
            grammar.wordCatInvList.get += Tuple2(term, HashSet[GrammarElement]())

          if (!grammar.wordCatInvList.get(term).contains(newVar))
            grammar.wordCatInvList.get(term) += newVar

        }

        // System.out.println("Node="+node+" Size="+size+" Found rightSide="+rightSide)

        Range(4, size).foreach(idx => headers ++= findRules(children(idx), grammar))

        // System.out.println("Node="+node+" Size="+size+" Found headers="+headers)
        return headers
      }
    }

  }

}

object TreeExtractor {

  def buildTree(rootState: State, ruleMap: HashMap[Int, State]): Tree = {
    var root = new Node(rootState.left)    
    root.i=rootState.i
    root.j=rootState.j
    var tree = new Tree(root)
    extract(root, rootState, ruleMap)
    tree
  }

  def extract(currentNode: Node, currentState: State, ruleMap: HashMap[Int, State]) {
    // System.out.println("Extracting from node " + currentNode.id)
    // System.out.println("Children " + currentState.children)
    currentState.children.isEmpty match {
      case false => {
        for (childrenId <- currentState.children) {
          // System.out.println("Inspecting child "+childrenId)
          var nextState=ruleMap(childrenId)
          var nextNode = new Node(nextState.left)
          nextNode.i=nextState.i
    	  nextNode.j=nextState.j
          currentNode.addChild(nextNode)
          extract(nextNode, ruleMap(childrenId), ruleMap)
        }
      }

      case true => {
        // System.out.println("Empty children for "+currentNode)
        var nextNode = new Node(currentState.right(0))
        currentNode.addChild(nextNode)
      }

    }

  }

}

