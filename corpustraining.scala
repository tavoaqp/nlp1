package ime.nlp

import scala.collection.mutable._
import scala.collection.generic._
import scala.util.control.Breaks._

object DeterministicTraining extends App{
  override def main(arr:Array[String]){
    CorpusTraining.runWithoutPCFG(Array[String]("aires-treino.parsed"))
  }
}

object ProbabilisticTraining extends App{
  override def main(arr:Array[String]){
    CorpusTraining.runWithPCFG(Array[String]("aires-treino.parsed"))
  }
}

object LaplaceProbabilisticTraining extends App{
  override def main(arr:Array[String]){
    CorpusTraining.runWithPCFGLaplace(Array[String]("aires-treino.parsed"))
  }
}

object CorpusTraining {

  var regexPunct = """\;|\:|\!|\?|\.|\,|\p{javaLowerCase}|\-""".r
  var regex = """\)|\(|\s|[a-zA-Z0-9çáéèíóúáãõâêôàüÉ]+([\-\+\/]*[a-zA-Z0-9çáéèíóúáãõâêôàü]*)*|\.|\,|\:|\;|\!|\?""".r

  var NONTERM=HashSet[String]("ADJ","ADJP","ADJX","ADV","ADVP","C","CL","COMMA","CONJ","CONJP","CONJS","CP","D","DEM","ET","FP","FRAG","HV","INTJ","IP","N","NEG","NP","NPR","NUM","NX","OUTRO","P","PERIOD","PP","PRO","PRO$","Q","SE","SENAO","SR","TR","VB","WADJP","WADV","WADVP","WD","WNP","WPP","WPRO","WQ",
    "P+D","VB+SE","P+DEM","P+PRO","VB+CL","P+ADV","CP/IP","HV+SE","P+WADV","SR+CL")

  var isNextCatPOSForPenn = new Function2[GrammarElement, Grammar, Boolean]() {
    def apply(nextCat: GrammarElement, grammar: Grammar): Boolean = {
      if (nextCat.isInstanceOf[Var] && (nextCat.asInstanceOf[Var].id == "NONTERM" || nextCat.asInstanceOf[Var].id == "TERM" || nextCat.asInstanceOf[Var].id == "RIGHTPAR" || nextCat.asInstanceOf[Var].id == "LEFTPAR")) {
        return true
      }

      return false
    }
  }

  var scannerPenn = new Function4[State, ArrayBuffer[Tuple2[ArrayBuffer[State],HashSet[State]]], Grammar, ArrayBuffer[String], Unit]() {

    def apply(state: State, chart: ArrayBuffer[Tuple2[ArrayBuffer[State],HashSet[State]]], grammar: Grammar, words: ArrayBuffer[String]): Unit = {
      if (state.j < words.size) {
        var jWord = words(state.j)
        
        var posTag: Option[GrammarElement] = None
        jWord match {
          case "(" => posTag = Some(Var("LEFTPAR"))
          case ")" => posTag = Some(Var("RIGHTPAR"))
          case other => {
            if (NONTERM.contains(jWord))
            {
              posTag=Some(Var("NONTERM"))
            
            }
            else{
              posTag=Some(Var("TERM"))
            
            }
            
          }
        }
        

        var nextCat = state.nextCategory
        if (nextCat == posTag.get) {
          var newState = State(None, nextCat, ElemList(Term(jWord)), 1, state.j, state.j + 1)
          if (!chart(state.j + 1)._2.contains(newState))
          {
            chart(state.j + 1)._1 += newState
            chart(state.j + 1)._2 += newState
            // System.out.println(newState + " SCANNER")
          }
          
        }
      }
    }

  }




  def runWithoutPCFG(arr: Array[String]) {

    var parser = new EarleyParser()
    var corpus = scala.io.Source.fromFile(arr(0), "utf-8").getLines.mkString.replaceAll("""\s+""", " ")
    var sequence = extractSequence(corpus)
    var pennGrammar = loadPennGrammar()

    for (testingIdx <- 0 to 10) {
      System.out.println("\nITERATION "+testingIdx)
      System.out.println("=====================")
      val minuteFormat = new java.text.SimpleDateFormat("dd-mm-yy hh:mm:ss")

      var now1=java.util.Calendar.getInstance()
      
      System.out.println("timeStart="+minuteFormat.format(now1.getTime()))

      var count = 0
      var buffer = ArrayBuffer[String]()      
      var testingTrees = ArrayBuffer[Tree]()
      var validationTrees = ArrayBuffer[Tree]()
      var rand = new scala.util.Random(scala.compat.Platform.currentTime)
      var seqCount=0

      System.out.println("Obtaining trees")
      for (word <- sequence) {
        buffer += word
        if (word == "(")
          count += 1
        else if (word == ")")
          count -= 1
        if (count == 0) {
          
          var probSelect = rand.nextFloat
          var trees = parser.parse(buffer, pennGrammar, isNextCatPOSForPenn, scannerPenn, EarleyParser.deterministicPredictor, EarleyParser.deterministicCompleter,EarleyParser.defaultTreeSelector)

          trees.isEmpty match {
            case false =>{
              if (probSelect <= 0.9)
                testingTrees ++= trees
              else
                validationTrees ++= trees
            }
            case true => {
              System.out.println("\nInput: "+buffer)
              System.out.println("TRAINING: Not recognized tree from corpus")
            }

          }

          buffer = ArrayBuffer[String]()
        }
        

      }
      // System.out.println("Testing trees size "+testingTrees.size)
      // System.out.println("Validation trees size "+validationTrees.size)
      
      System.out.println("Obtaining corpus grammar")
      var corpusGrammar = GrammarExtractor.extract(testingTrees)      
      System.out.println("Obtaining validation grammar")
      var validationGrammar = GrammarExtractor.extract(validationTrees)
      
      var positiveRec = 0
      var negativeRec = 0
      var totalTrees = testingTrees.size + validationTrees.size
      

      var fullValidationSentences=ArrayBuffer[ArrayBuffer[String]]()

      var validationSentences=validationTrees.map( t => t.getSentence).flatMap( t => t)

      var validIdx=0
      var currBuffer=ArrayBuffer[String]()
      while( validIdx < validationSentences.size){        
        var currWord=validationSentences(validIdx)

        currBuffer+=currWord
        if (currWord.equals(".") || currWord.equals(",") || currWord.equals(";"))
        {          
          fullValidationSentences+=currBuffer
          currBuffer=ArrayBuffer[String]()
        }         
        
        validIdx+=1
      }

      // System.out.println("Validation sentences size "+fullValidationSentences.size)

      System.out.println("\nRodando teste (Iteração "+testingIdx+")")
      System.out.println("-------------------------------------------")

      var numProcessors=Runtime.getRuntime().availableProcessors()

      var testingResults=ParallelParsing.calculate(numProcessors, fullValidationSentences.sortWith( (a,b) => a.size < b.size), corpusGrammar, true)
      
      var positiveSentences=testingResults._1
      positiveRec=positiveSentences.size
      negativeRec=testingResults._2.size
      
      if (positiveSentences.size>0)
      {
        var truePositive = 0
        var falsePositive = 0
        System.out.println("\nRodando validacao (Iteração "+testingIdx+")")
        System.out.println("-----------------------------------------------")
        
        var validPositiveResults=ParallelParsing.calculate(numProcessors, positiveSentences.map( t => t._1).sortWith((a,b) => a.size < b.size), validationGrammar, true)

        var fullValidationResults=ParallelParsing.calculate(numProcessors, fullValidationSentences.sortWith((a,b) => a.size < b.size), validationGrammar, true)

        truePositive=validPositiveResults._1.size
        falsePositive=validPositiveResults._2.size

        System.out.println("\nResultados Iteração " + testingIdx)
        System.out.println("-------------------------------------")
        System.out.println("(Iteration "+testingIdx+") POSITIVE RECOGNITIONS = " + positiveRec)
        System.out.println("(Iteration "+testingIdx+") NEGATIVE RECOGNITIONS = " + negativeRec)
        System.out.println("(Iteration "+testingIdx+") TRUE POSITIVE = " + truePositive)
        System.out.println("(Iteration "+testingIdx+") FALSE POSITIVE = " + falsePositive)
        System.out.println("(Iteration "+testingIdx+") TOTAL TREES = " + totalTrees)
        System.out.println("(Iteration "+testingIdx+") RECALL = " + (truePositive.toFloat / fullValidationResults._1.size.toFloat))
        System.out.println("(Iteration "+testingIdx+") PRECISION = " + (truePositive.toFloat / positiveRec.toFloat))
      }

      var now2=java.util.Calendar.getInstance()
      
      System.out.println("(Iteration "+testingIdx+") timeEnd="+minuteFormat.format(now2.getTime()))
      System.out.println("(Iteration "+testingIdx+") totalTime in Minutes="+(now2.getTimeInMillis() - now1.getTimeInMillis()).toFloat/(1000*60))
    }
  }


  def runWithPCFG(arr: Array[String]) {
    var parser = new EarleyParser()
    var corpus = scala.io.Source.fromFile(arr(0), "utf-8").getLines.mkString.replaceAll("""\s+""", " ")
    var sequence = extractSequence(corpus)
    var pennGrammar = loadPennGrammar()

    for (testingIdx <- 0 to 10) {
      System.out.println("\nITERATION "+testingIdx)
      System.out.println("=====================")
      val minuteFormat = new java.text.SimpleDateFormat("dd-mm-yy hh:mm:ss")

      var now1=java.util.Calendar.getInstance()
      
      System.out.println("timeStart="+minuteFormat.format(now1.getTime()))

      var count = 0
      var buffer = ArrayBuffer[String]()      
      var testingTrees = ArrayBuffer[Tree]()
      var validationTrees = ArrayBuffer[Tree]()
      var rand = new scala.util.Random(scala.compat.Platform.currentTime)
      var seqCount=0

      System.out.println("Obtaining trees")
      for (word <- sequence) {
        buffer += word
        if (word == "(")
          count += 1
        else if (word == ")")
          count -= 1
        if (count == 0) {
          
          var probSelect = rand.nextFloat
          var trees = parser.parse(buffer, pennGrammar, isNextCatPOSForPenn, scannerPenn, EarleyParser.deterministicPredictor, EarleyParser.deterministicCompleter,EarleyParser.defaultTreeSelector)

          trees.isEmpty match {
            case false =>{
              if (probSelect <= 0.9)
                testingTrees ++= trees
              else
                validationTrees ++= trees
            }
            case true => {
              System.out.println("\nInput: "+buffer)
              System.out.println("TRAINING: Not recognized tree from corpus")
            }

          }

          buffer = ArrayBuffer[String]()
        }
        

      }
      // System.out.println("Testing trees size "+testingTrees.size)
      // System.out.println("Validation trees size "+validationTrees.size)
      
      System.out.println("Obtaining corpus grammar")
      var corpusGrammar = GrammarExtractor.extractProbabilistic(testingTrees)
      
      System.out.println("Obtaining validation grammar")
      var validationGrammar = GrammarExtractor.extractProbabilistic(validationTrees)
      
      var positiveRec = 0
      var negativeRec = 0
      var totalTrees = testingTrees.size + validationTrees.size
      var positiveSentences = ArrayBuffer[Tuple2[ArrayBuffer[String],ArrayBuffer[Tree]]]()

      var fullValidationSentences=ArrayBuffer[ArrayBuffer[String]]()

      var validationSentences=validationTrees.map( t => t.getSentence).flatMap( t => t)

      var validIdx=0
      var currBuffer=ArrayBuffer[String]()
      while( validIdx < validationSentences.size){        
        var currWord=validationSentences(validIdx)

        currBuffer+=currWord
        if (currWord.equals(".") || currWord.equals(",") || currWord.equals(";"))
        {          
          fullValidationSentences+=currBuffer
          currBuffer=ArrayBuffer[String]()
        }         
        
        validIdx+=1
      }

      System.out.println("Validation sentences size "+fullValidationSentences.size)

      System.out.println("\nRodando teste (Iteração "+testingIdx+")")
      System.out.println("-------------------------------------------")

      var numProcessors=Runtime.getRuntime().availableProcessors()

      var testingResults=ParallelParsing.calculate(numProcessors, fullValidationSentences.sortWith( (a,b) => a.size < b.size), corpusGrammar, false)
      
      positiveSentences=testingResults._1
      var positiveSenteMap=positiveSentences.toMap
      positiveRec=positiveSentences.size
      negativeRec=testingResults._2.size
      
      // System.out.println("positiveRec="+positiveRec)
      // System.out.println("negativeRec="+negativeRec)

      if (positiveSentences.size>0)
      {
        var truePositive = 0
        var falsePositive = 0
        System.out.println("\nRodando validacao (Iteração "+testingIdx+")")
        System.out.println("-----------------------------------------------")
        
        var validationResults=ParallelParsing.calculate(numProcessors, positiveSentences.map(t => t._1).sortWith((a,b) => a.size < b.size), validationGrammar, false)      

        var fullValidationResults=ParallelParsing.calculate(numProcessors, fullValidationSentences.sortWith((a,b) => a.size < b.size), validationGrammar, false)      

        truePositive=validationResults._1.size
        var truePositiveMap=validationResults._1.toMap
        falsePositive=validationResults._2.size

        var totalPositiveConst=0
        var totalValidConst=0

        var totalStrictPositiveConst=0
        var totalStrictValidConst=0

        truePositiveMap.keys.foreach ( t => {
            var truePosConstituents = truePositiveMap(t)(0).getConstituents._1
            var positiveConstituents = positiveSenteMap(t)(0).getConstituents._1            

            var trueStrictPosConstituents = truePositiveMap(t)(0).getConstituents._2
            var positiveStrictConstituents = positiveSenteMap(t)(0).getConstituents._2            

            totalPositiveConst+=positiveConstituents.size
            totalValidConst+=(truePosConstituents & positiveConstituents).size

            totalStrictPositiveConst+=positiveStrictConstituents.size
            totalStrictValidConst+=(trueStrictPosConstituents & positiveStrictConstituents).size
          })
        //measure parenthesis precision




        System.out.println("\nResultados Iteração " + testingIdx)
        System.out.println("-------------------------------------")
        System.out.println("(Iteration "+testingIdx+") POSITIVE RECOGNITIONS = " + positiveRec)
        System.out.println("(Iteration "+testingIdx+") NEGATIVE RECOGNITIONS = " + negativeRec)
        System.out.println("(Iteration "+testingIdx+") TRUE POSITIVE = " + truePositive)
        System.out.println("(Iteration "+testingIdx+") FALSE POSITIVE = " + falsePositive)
        System.out.println("(Iteration "+testingIdx+") TOTAL TREES = " + totalTrees)
        var recall=(positiveRec.toFloat / fullValidationResults._1.size.toFloat)
        System.out.println("(Iteration "+testingIdx+") RECALL = " + recall)
        var precision=(truePositive.toFloat / positiveRec.toFloat)
        System.out.println("(Iteration "+testingIdx+") PRECISION = " + precision)
        System.out.println("(Iteration "+testingIdx+") PARENTHENTIZATION PRECISION = " + (totalValidConst.toFloat / totalPositiveConst.toFloat))
        System.out.println("(Iteration "+testingIdx+") STRICT PARENTHENTIZATION PRECISION = " + (totalStrictValidConst.toFloat / totalStrictPositiveConst.toFloat))
        System.out.println("(Iteration "+testingIdx+") F-MEASURE = " + (2*recall*precision/ (recall+precision)))
      }

      var now2=java.util.Calendar.getInstance()
      
      System.out.println("(Iteration "+testingIdx+") timeEnd="+minuteFormat.format(now2.getTime()))
      System.out.println("(Iteration "+testingIdx+") totalTime in Minutes="+(now2.getTimeInMillis()-now1.getTimeInMillis()).toFloat/(1000*60))
    }

  }


  def runWithPCFGLaplace(arr: Array[String]) {
    var parser = new EarleyParser()
    var corpus = scala.io.Source.fromFile(arr(0), "utf-8").getLines.mkString.replaceAll("""\s+""", " ")
    var sequence = extractSequence(corpus)
    var pennGrammar = loadPennGrammar()

    for (testingIdx <- 0 to 10) {
      System.out.println("\nITERATION "+testingIdx)
      System.out.println("=====================")
      val minuteFormat = new java.text.SimpleDateFormat("dd-mm-yy hh:mm:ss")

      var now1=java.util.Calendar.getInstance()
      
      System.out.println("timeStart="+minuteFormat.format(now1.getTime()))

      var count = 0
      var buffer = ArrayBuffer[String]()      
      var testingTrees = ArrayBuffer[Tree]()
      var validationTrees = ArrayBuffer[Tree]()
      var rand = new scala.util.Random(scala.compat.Platform.currentTime)
      var seqCount=0

      System.out.println("Obtaining trees")
      for (word <- sequence) {
        buffer += word
        if (word == "(")
          count += 1
        else if (word == ")")
          count -= 1
        if (count == 0) {
          
          var probSelect = rand.nextFloat
          var trees = parser.parse(buffer, pennGrammar, isNextCatPOSForPenn, scannerPenn, EarleyParser.deterministicPredictor, EarleyParser.deterministicCompleter,EarleyParser.defaultTreeSelector)

          trees.isEmpty match {
            case false =>{
              if (probSelect <= 0.9)
                testingTrees ++= trees
              else
                validationTrees ++= trees
            }
            case true => {
              System.out.println("\nInput: "+buffer)
              System.out.println("TRAINING: Not recognized tree from corpus")
            }

          }

          buffer = ArrayBuffer[String]()
        }
        

      }
      // System.out.println("Testing trees size "+testingTrees.size)
      // System.out.println("Validation trees size "+validationTrees.size)
      
      System.out.println("Obtaining corpus grammar")
      var corpusGrammar = GrammarExtractor.extractProbabilisticWithLaplace(testingTrees)
      
      System.out.println("Obtaining validation grammar")
      var validationGrammar = GrammarExtractor.extractProbabilisticWithLaplace(validationTrees)
      
      var positiveRec = 0
      var negativeRec = 0
      var totalTrees = testingTrees.size + validationTrees.size
      var positiveSentences = ArrayBuffer[Tuple2[ArrayBuffer[String],ArrayBuffer[Tree]]]()

      var fullValidationSentences=ArrayBuffer[ArrayBuffer[String]]()

      var validationSentences=validationTrees.map( t => t.getSentence).flatMap( t => t)

      var validIdx=0
      var currBuffer=ArrayBuffer[String]()
      while( validIdx < validationSentences.size){        
        var currWord=validationSentences(validIdx)

        currBuffer+=currWord
        if (currWord.equals(".") || currWord.equals(",") || currWord.equals(";"))
        {          
          fullValidationSentences+=currBuffer
          currBuffer=ArrayBuffer[String]()
        }         
        
        validIdx+=1
      }

      System.out.println("Validation sentences size "+fullValidationSentences.size)

      System.out.println("\nRodando teste (Iteração "+testingIdx+")")
      System.out.println("-------------------------------------------")

      var numProcessors=Runtime.getRuntime().availableProcessors()

      var testingResults=ParallelParsing.calculate(numProcessors, fullValidationSentences.sortWith( (a,b) => a.size < b.size), corpusGrammar, false)
      
      positiveSentences=testingResults._1
      var positiveSenteMap=positiveSentences.toMap
      positiveRec=positiveSentences.size
      negativeRec=testingResults._2.size
      
      // System.out.println("positiveRec="+positiveRec)
      // System.out.println("negativeRec="+negativeRec)

      if (positiveSentences.size>0)
      {
        var truePositive = 0
        var falsePositive = 0
        System.out.println("\nRodando validacao (Iteração "+testingIdx+")")
        System.out.println("-----------------------------------------------")
        
        var validationResults=ParallelParsing.calculate(numProcessors, positiveSentences.map(t => t._1).sortWith((a,b) => a.size < b.size), validationGrammar, false)      

        var fullValidationResults=ParallelParsing.calculate(numProcessors, fullValidationSentences.sortWith((a,b) => a.size < b.size), validationGrammar, false)      

        truePositive=validationResults._1.size
        var truePositiveMap=validationResults._1.toMap
        falsePositive=validationResults._2.size

        var totalPositiveConst=0
        var totalValidConst=0

        var totalStrictPositiveConst=0
        var totalStrictValidConst=0

        truePositiveMap.keys.foreach ( t => {
            var truePosConstituents = truePositiveMap(t)(0).getConstituents._1
            var positiveConstituents = positiveSenteMap(t)(0).getConstituents._1            

            var trueStrictPosConstituents = truePositiveMap(t)(0).getConstituents._2
            var positiveStrictConstituents = positiveSenteMap(t)(0).getConstituents._2            

            totalPositiveConst+=positiveConstituents.size
            totalValidConst+=(truePosConstituents & positiveConstituents).size

            totalStrictPositiveConst+=positiveStrictConstituents.size
            totalStrictValidConst+=(trueStrictPosConstituents & positiveStrictConstituents).size
          })
        //measure parenthesis precision




        System.out.println("\nResultados Iteração " + testingIdx)
        System.out.println("-------------------------------------")
        System.out.println("(Iteration "+testingIdx+") POSITIVE RECOGNITIONS = " + positiveRec)
        System.out.println("(Iteration "+testingIdx+") NEGATIVE RECOGNITIONS = " + negativeRec)
        System.out.println("(Iteration "+testingIdx+") TRUE POSITIVE = " + truePositive)
        System.out.println("(Iteration "+testingIdx+") FALSE POSITIVE = " + falsePositive)
        System.out.println("(Iteration "+testingIdx+") TOTAL TREES = " + totalTrees)
        var recall=(positiveRec.toFloat / fullValidationResults._1.size.toFloat)
        System.out.println("(Iteration "+testingIdx+") RECALL = " + recall)
        var precision=(truePositive.toFloat / positiveRec.toFloat)
        System.out.println("(Iteration "+testingIdx+") PRECISION = " + precision)
        System.out.println("(Iteration "+testingIdx+") PARENTHENTIZATION PRECISION = " + (totalValidConst.toFloat / totalPositiveConst.toFloat))
        System.out.println("(Iteration "+testingIdx+") STRICT PARENTHENTIZATION PRECISION = " + (totalStrictValidConst.toFloat / totalStrictPositiveConst.toFloat))
        System.out.println("(Iteration "+testingIdx+") F-MEASURE = " + (2*recall*precision/ (recall+precision)))
      }

      var now2=java.util.Calendar.getInstance()
      
      System.out.println("(Iteration "+testingIdx+") timeEnd="+minuteFormat.format(now2.getTime()))
      System.out.println("(Iteration "+testingIdx+") totalTime in Minutes="+(now2.getTimeInMillis()-now1.getTimeInMillis()).toFloat/(1000*60))
    }

  }

  def runWithoutPCFGDifficult(arr: Array[String]) {

    var parser = new EarleyParser()

    var corpus = scala.io.Source.fromFile(arr(0), "utf-8").getLines.mkString.replaceAll("""\s+""", " ")
    var sequence = extractSequence(corpus)
    var pennGrammar = loadPennGrammar()

    
    
      System.out.println("=====================")
      val minuteFormat = new java.text.SimpleDateFormat("dd-mm-yy hh:mm:ss")

      var now1=java.util.Calendar.getInstance()
      
      System.out.println("timeStart="+minuteFormat.format(now1.getTime()))

      var count = 0
      var buffer = ArrayBuffer[String]()      
      var testingTrees = ArrayBuffer[Tree]()
      var validationTrees = ArrayBuffer[Tree]()
      var rand = new scala.util.Random(scala.compat.Platform.currentTime)
      var seqCount=0

      System.out.println("Obtaining trees")
      for (word <- sequence) {
        buffer += word
        if (word == "(")
          count += 1
        else if (word == ")")
          count -= 1
        if (count == 0) {
          
          var probSelect = rand.nextFloat
          var trees = parser.parse(buffer, pennGrammar, isNextCatPOSForPenn, scannerPenn, EarleyParser.deterministicPredictor, EarleyParser.deterministicCompleter,EarleyParser.defaultTreeSelector)

          trees.isEmpty match {
            case false =>{
              if (probSelect <= 0.8)
                testingTrees ++= trees
              else
                validationTrees ++= trees
            }
            case true => {
              System.out.println("\nInput: "+buffer)
              System.out.println("TRAINING: Not recognized tree from corpus")
            }

          }

          buffer = ArrayBuffer[String]()
        }
        

      }
      System.out.println("Testing trees size "+testingTrees.size)
      System.out.println("Validation trees size "+validationTrees.size)
      
      System.out.println("Obtaining corpus grammar")
      var corpusGrammar = GrammarExtractor.extract(testingTrees)   
      // System.out.println(corpusGrammar)   
      System.out.println("Obtaining validation grammar")
      var validationGrammar = GrammarExtractor.extract(validationTrees)
      
      var numProcessors=Runtime.getRuntime().availableProcessors()

      var fullValidationSentences=ArrayBuffer[ArrayBuffer[String]]()
      fullValidationSentences+=ArrayBuffer[String]("todas","são","iguais","e","todas","grandes")

      var testingResults=ParallelParsing.calculate(numProcessors, fullValidationSentences, corpusGrammar, true)
      
      
      var now2=java.util.Calendar.getInstance()
      
      System.out.println("timeEnd="+minuteFormat.format(now2.getTime()))
      System.out.println("totalTime in Minutes="+(now2.getTimeInMillis() - now1.getTimeInMillis()).toFloat/(1000*60))
      System.out.println(testingResults)
  }


  def extractSequence(corpus: String): ArrayBuffer[String] = {
    var sequence = ArrayBuffer[String]()

    var splits = regex.findAllIn(corpus)
    while (splits.hasNext) {
      var n = splits.next
      if (n != " ") {
        sequence += n
      }

    }
    sequence
  }

  def loadPennGrammar(): Grammar = {
    var pennGrammar = Grammar()
    pennGrammar += Tuple2(Var("START"), ElemListArray())
    pennGrammar += Tuple2(Var("LIST"), ElemListArray())
    pennGrammar += Tuple2(Var("TERM"), ElemListArray())
    pennGrammar += Tuple2(Var("NONTERM"), ElemListArray())
    pennGrammar += Tuple2(Var("LEFTPAR"), ElemListArray(ElemList(Term("("))))
    pennGrammar += Tuple2(Var("RIGHTPAR"), ElemListArray(ElemList(Term(")"))))

    pennGrammar(Var("START")) += ElemList(Var("LEFTPAR"), Var("NONTERM"), Var("LIST"), Var("RIGHTPAR"))
    pennGrammar(Var("LIST")) += ElemList(Var("LEFTPAR"), Var("NONTERM"), Var("LIST"), Var("RIGHTPAR"), Var("LIST"))
    pennGrammar(Var("LIST")) += ElemList(Var("LEFTPAR"), Var("NONTERM"), Var("LIST"), Var("RIGHTPAR"))
    pennGrammar(Var("LIST")) += ElemList(Var("TERM"))

    pennGrammar
  }

}

