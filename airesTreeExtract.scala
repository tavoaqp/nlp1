package ime.nlp

import scala.collection.mutable._
import scala.collection.generic._

object AiresTreeExtract{

	var regexPunct="""\;|\:|\!|\?|\.|\,|\p{javaLowerCase}""".r  
	var regex="""\)|\(|\s|([a-zA-Z0-9çáéíóúáãõâêôàü]\+*[a-zA-Z0-9çáéíóúáãõâêôàü]*)+|\.|\,|\:|\;""".r
	
	var isNextCatPOSForPenn=new Function2[GrammarElement, Grammar, Boolean](){
      def apply(nextCat: GrammarElement, grammar: Grammar):Boolean={
        if (nextCat.isInstanceOf[Var] && (nextCat.asInstanceOf[Var].id=="NONTERM" || nextCat.asInstanceOf[Var].id=="TERM" || nextCat.asInstanceOf[Var].id=="RIGHTPAR" || nextCat.asInstanceOf[Var].id=="LEFTPAR"))
        {
          return true
        }

        return false
      }
    }

    var scannerPenn=new Function4[State, ArrayBuffer[Tuple2[ArrayBuffer[State],HashSet[State]]], Grammar, ArrayBuffer[String], Unit](){

      def apply(state:State, chart: ArrayBuffer[Tuple2[ArrayBuffer[State],HashSet[State]]], grammar:Grammar, words:ArrayBuffer[String]):Unit={
        if (state.j < words.size)
        {
          var jWord=words(state.j)
          var posTag:Option[GrammarElement]=None
          jWord match {
            case "(" => posTag=Some(Var("LEFTPAR"))
            case ")" => posTag=Some(Var("RIGHTPAR"))
            case other => {
              var splits=regexPunct.findAllIn(jWord)
              if (splits.hasNext)
              {
                posTag=Some(Var("TERM")) 
              }
              else
              {
                posTag=Some(Var("NONTERM"))
              }
            }        
          }
          System.out.println("word="+jWord+" category="+posTag)

          var nextCat=state.nextCategory
          if (nextCat==posTag.get){
            var newState=State(Some(1),nextCat, ElemList(Term(jWord)), 1, state.j, state.j+1)
            if (!chart(state.j+1)._2.contains(newState))
              chart(state.j+1)._1+=newState
              chart(state.j+1)._2+=newState
              System.out.println("SCANNER added "+newState+" to chart("+(state.j+1)+") FROM "+state)
          }
        }
      }
      
    }

	def run(arr:Array[String]){
      // var corpus1=scala.io.Source.fromFile(arr(0),"utf-8").getLines.toArray
      // System.out.println(corpus1)
      var parser=new EarleyParser()
      var corpus=scala.io.Source.fromFile(arr(0),"utf-8").getLines.mkString.replaceAll("""\s+"""," ")
      var sequence=extractSequence(corpus)

      var count=0
      var buffer=ArrayBuffer[String]()
      var pennGrammar=loadPennGrammar()
      for (word <- sequence){
        if (word=="(")
          count+=1
        else if (word==")")
          count-=1
        if (count==0)
        {
          parser.parse(buffer,pennGrammar, isNextCatPOSForPenn, scannerPenn, EarleyParser.deterministicPredictor, EarleyParser.deterministicCompleter,EarleyParser.defaultTreeSelector)
          buffer=ArrayBuffer[String]()
        }
        buffer+=word

      }
      
    }

    def extractSequence(corpus:String):ArrayBuffer[String]={
        var sequence=ArrayBuffer[String]()

        var splits=regex.findAllIn(corpus)
        while( splits.hasNext)
        {        
          var n=splits.next
          if (n!=" ")          
          {
            sequence+=n
          }        
          
        }
        sequence
    }

    def loadPennGrammar():Grammar={
      var pennGrammar=Grammar()
      pennGrammar+=Tuple2(Var("START"),ElemListArray())      
      pennGrammar+=Tuple2(Var("LIST"),ElemListArray())
      pennGrammar+=Tuple2(Var("TERM"),ElemListArray())
      pennGrammar+=Tuple2(Var("NONTERM"),ElemListArray())
      pennGrammar+=Tuple2(Var("LEFTPAR"),ElemListArray(ElemList(Term("("))))
      pennGrammar+=Tuple2(Var("RIGHTPAR"),ElemListArray(ElemList(Term(")"))))

      pennGrammar(Var("START"))+=ElemList(Var("LEFTPAR"),Var("NONTERM"),Var("LIST"),Var("RIGHTPAR"))
      pennGrammar(Var("LIST"))+=ElemList(Var("LEFTPAR"),Var("NONTERM"),Var("LIST"),Var("RIGHTPAR"),Var("LIST"))
      pennGrammar(Var("LIST"))+=ElemList(Var("LEFTPAR"),Var("NONTERM"),Var("LIST"),Var("RIGHTPAR"))
      pennGrammar(Var("LIST"))+=ElemList(Var("TERM"))

      pennGrammar
    }

}