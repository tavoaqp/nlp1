package ime.nlp

import scala.collection.mutable._
import scala.collection.generic._

abstract class GrammarElement

case class Var(id:String) extends GrammarElement
{
  override def toString():String={
    // "VAR "+id
    id
  }
}
case class Term(id:String) extends GrammarElement{
  override def toString():String={
    // "TERM "+id
    id
  } 
}

class ElemList(var prob:Option[Double]=None) extends ArrayBuffer[GrammarElement]
{
 override def toString():String={
    var buf=""
    this.foreach{ case t => buf+=t.toString+" "}
    buf.substring(0,math.max(buf.size-1,0))
    buf+=" Prob="+prob
    buf
  } 
}
object ElemList{
  def apply(elems:GrammarElement*):ElemList=(new ElemList())++=elems
  def apply(prob:Double,elems:GrammarElement*):ElemList=(new ElemList(Some(prob)))++=elems
}

class ElemListArray extends ArrayBuffer[ElemList]

object ElemListArray{
  def apply(elems:ElemList*):ElemListArray=(new ElemListArray())++=elems
}

case class Grammar(var wordCatInvList:Option[HashMap[String,HashSet[GrammarElement]]]=None) extends HashMap[GrammarElement,ArrayBuffer[ElemList]]
{
  override def toString():String={
    var buffer=""
    for (pair <- this){
      pair._2.foreach ( u => buffer+=pair._1+"->"+u.toString+"\n" )
      
    }

    buffer+="\nwordCatInvList\n"
    buffer+="==============\n"
    // if (!wordCatInvList.isEmpty)
    // {
    //   wordCatInvList.get.keys.foreach ( k => buffer+=k+"->"+wordCatInvList.get(k)+"\n")
    // }
    wordCatInvList.foreach ( t => t.foreach( u => buffer+=u+"\n"))
    return buffer
  }
}

case class State(var id:Option[Int]=None,var left:GrammarElement, var right:ElemList, var marker:Int, var i:Int, var j:Int, var children:ArrayBuffer[Int]=ArrayBuffer[Int](), var score:Option[Double]=None)
{


  def complete():Boolean={    
    marker==right.size
  }

  def addChildren(child:Int){
    children+=child
  }

  def advanceMark():State={
    // State(left,right, math.max(marker+1, right.size), i,j)
    if (marker>=right.size)
      State(None,left,right, right.size, i,j, ArrayBuffer[Int]()++=children, score)
    else
      State(None,left,right, marker+1, i,j, ArrayBuffer[Int]()++=children, score)
  }

  def nextCategory():GrammarElement={
    // System.out.println("Inside nextCategory {"+left+" -- "+right+" marker="+marker+" i="+i+" j="+j)
    if (marker < right.size)
      right(marker)
    else 
      right(marker-1)
  }

  override def equals(other:Any):Boolean={
    other match{
      case that: State =>
        left.equals(that.left) && right.equals(that.right) && i.equals(that.i) && j.equals(that.j) && marker.equals(that.marker)
      case _ => false
    }
  }

  override def hashCode():Int={
    var hash=1
    hash=hash*19 + left.hashCode
    hash=hash*444553 + right.hashCode
    hash=hash*15485863 + i.hashCode
    hash=hash*4496353 + j.hashCode
    hash=hash*9299183 + marker.hashCode
    hash
  }

  override def toString():String={
    var id=""
    var buf=""
    if (left.isInstanceOf[Var]){
      id=left.asInstanceOf[Var].id
    }else
    {
      id=left.asInstanceOf[Term].id
    }
    buf="{[id="+this.id+"] ["+id+" -> "
    for (elem <- right){
      var elemId=""
      if (elem.isInstanceOf[Var]){
        elemId=elem.asInstanceOf[Var].id
      }else
      {
        elemId=elem.asInstanceOf[Term].id
      } 
      buf+=elemId+" "
    }

    buf+="] [@="+marker+"] [nextCat="+nextCategory().toString()+"] ["+i+","+j+"] [children="+children+"] [score="+score+"]}"
    return buf
  }
}


class Node(var id:GrammarElement, var children:Option[ArrayBuffer[Node]]=Some(ArrayBuffer[Node]()), var prob:Option[Double]=None, var i:Int=0, var j:Int=0) {
  def addChild(node:Node){
    children.get+=node
  }

  override def toString():String={
    id.toString+" i="+i+" j="+j
 }
}

class Tree(var root:Node){
  def getSentence():ArrayBuffer[String]=SentenceExtractor.extractSentence(this)

  override def toString():String={
    var buffer=""
    var stack=new Stack[Tuple2[Int,Node]]()
    stack.push(Tuple2(0,root))

    while(!stack.isEmpty){
      var nextPair=stack.pop
      Range(0,nextPair._1).foreach ( t => buffer+="    ")
      buffer+=nextPair._2.toString+"\n"
      // System.out.println("Node "+nextPair._2+" children "+nextPair._2.children)
      nextPair._2.children.foreach ( t => t.map (u => Tuple2(nextPair._1+1,u)).reverse.foreach (  v =>stack.push(v)) )
    }
    return buffer
  }

  def getConstituents():Tuple2[HashSet[ShallowSyntagma],HashSet[DeepSyntagma]]={
    var stack=new Queue[Node]()
    stack+=root
    var output1=HashSet[ShallowSyntagma]()
    var output2=HashSet[DeepSyntagma]()
    while (!stack.isEmpty){
      var nextNode=stack.dequeue
      nextNode.id match {
        case Var(_) => {
          output1+=new ShallowSyntagma(nextNode.id.asInstanceOf[Var], nextNode.i, nextNode.j)
          output2+=new DeepSyntagma(nextNode.id.asInstanceOf[Var], nextNode.i, nextNode.j)
          nextNode.children.foreach ( t => stack++=t)
        }
        case _ =>
      }
    }

    Tuple2(output1, output2)

  }
}


class ShallowSyntagma(var id:Var, var i:Int, var j:Int){

  override def equals(other:Any):Boolean={
    other match{
      case that: ShallowSyntagma => i==that.i && j==that.j
      case _ => false
    }
  }

  override def hashCode():Int={
    var hash=1
    hash=hash*15485863 + i.hashCode
    hash=hash*4496353 + j.hashCode    
    hash
  }

  override def toString():String={
    id.toString+" i="+i+" j="+j
  }

}

class DeepSyntagma(var id:Var, var i:Int, var j:Int){

  override def equals(other:Any):Boolean={
    other match{
      case that: ShallowSyntagma => that.id.equals(id) && i==that.i && j==that.j
      case _ => false
    }
  }

  override def hashCode():Int={
    var hash=1
    hash=hash*19 + id.hashCode
    hash=hash*15485863 + i.hashCode
    hash=hash*4496353 + j.hashCode    
    hash
  }

  override def toString():String={
    id.toString+" i="+i+" j="+j
  }

}

