package ime.nlp

import akka.actor._
import akka.routing.RoundRobinRouter
import scala.collection.mutable._

object ParallelParsing  {
 
  // calculate(nrOfWorkers = 4, nrOfElements = 10000, nrOfMessages = 10000)
 
  sealed trait ParseMessage
  case class Parse(deterministic:Boolean) extends ParseMessage
  case class Work(sentence: ArrayBuffer[String], grammar: Grammar, deterministic:Boolean) extends ParseMessage
  case class Result(sentence:ArrayBuffer[String],trees:ArrayBuffer[Tree]) extends ParseMessage
  case class ParseResult(positiveSentences:ArrayBuffer[Tuple2[ArrayBuffer[String],ArrayBuffer[Tree]]], negativeSentences:ArrayBuffer[ArrayBuffer[String]])
 
  class Worker extends Actor {
 
    def parseSentence(sentence: ArrayBuffer[String], grammar: Grammar, deterministic:Boolean): ArrayBuffer[Tree] = {
    	// System.out.println("Parsing sentence "+sentence)
    	var parser = new EarleyParser()

    	var trees=ArrayBuffer[Tree]()
    	if (deterministic)
    	{
    		trees ++= parser.parse(sentence, grammar, EarleyParser.isNextCatPOS, EarleyParser.scanner, EarleyParser.deterministicPredictor, 
    			EarleyParser.deterministicCompleter,EarleyParser.defaultTreeSelector)	
    	}
    	else{
    		trees ++= parser.parse(sentence, grammar, EarleyParser.isNextCatPOS, EarleyParser.probabilisticScanner, EarleyParser.probabilisticPredictor, EarleyParser.probabilisticCompleter,EarleyParser.maxProbTreeSelector)		
    	}

    	
    	// System.out.println("Sentence "+sentence+" has "+trees.size+" trees")
    	// System.out.println("Sentence "+sentence+" has "+trees)

    	return trees
    }
 
    def receive = {
      case Work(sentence, grammar, deterministic) =>
        sender ! Result(sentence, parseSentence(sentence, grammar, deterministic)) // perform the work
    }
  }
 
  class Master(nrOfWorkers: Int, sentences:ArrayBuffer[ArrayBuffer[String]], grammar:Grammar,listener: ActorRef)
    extends Actor {
 
    var positiveCount: Int = _
    var negativeCount: Int = _
    var nrOfResults: Int = _
    var positiveSentences=ArrayBuffer[Tuple2[ArrayBuffer[String],ArrayBuffer[Tree]]]()
    var negativeSentences=ArrayBuffer[ArrayBuffer[String]]()
 
    val workerRouter = context.actorOf(
      Props[Worker].withRouter(RoundRobinRouter(nrOfWorkers)), name = "workerRouter")
 
    def receive = {
      case Parse(deterministic) =>
      {      	
        for (sentence <- sentences) workerRouter ! Work(sentence, grammar,deterministic)
      }
      case Result(sentence, trees) =>
        
        !trees.isEmpty match{
        	case true => {
        		positiveSentences+=Tuple2(sentence, trees)
        	}
        	case false => negativeSentences+=sentence
        }
        nrOfResults+=1
        if (nrOfResults % 20 == 0)	
        	System.out.println("Parsed "+nrOfResults+" senteces")

        if (nrOfResults == sentences.size) {
          listener ! ParseResult(positiveSentences, negativeSentences)
          context.stop(self)
          
        }
    }
 
  }
  
 class Listener(val positiveSentences:ArrayBuffer[Tuple2[ArrayBuffer[String],ArrayBuffer[Tree]]],  val negativeSentences: ArrayBuffer[ArrayBuffer[String]]) extends Actor {
    def receive = {
      case ParseResult(positiveSentences, negativeSentence) =>

        this.positiveSentences++=positiveSentences        
        this.negativeSentences++=negativeSentence
        // System.out.println("Listener: positiveCount="+this.positiveSentences.size)        		
        // System.out.println("Listener: negativeCount="+this.negativeSentences.size)        		
        context.system.shutdown()
    }
  }

 
 
	def calculate(nrOfWorkers: Int, sentences:ArrayBuffer[ArrayBuffer[String]], grammar:Grammar, deterministic:Boolean):Tuple2[ArrayBuffer[Tuple2[ArrayBuffer[String],ArrayBuffer[Tree]]],ArrayBuffer[ArrayBuffer[String]]]={
		val system = ActorSystem("ParserSystem")
		val positiveSentences=ArrayBuffer[Tuple2[ArrayBuffer[String],ArrayBuffer[Tree]]]()
		val negativeSentences=ArrayBuffer[ArrayBuffer[String]]()
		

		val listener = system.actorOf(Props(new Listener(positiveSentences, negativeSentences)), name = "listener")
		val master = system.actorOf(Props(new Master(
	      nrOfWorkers, sentences, grammar, listener)),
	      name = "master")
	 
	    // start the calculation
	    master ! Parse(deterministic)
	    system.awaitTermination
	    
	    return Tuple2(positiveSentences, negativeSentences)
	}

 
}