package ime.nlp
import scala.collection.mutable._

object TestEarley{

	def run(){
		var parser=new EarleyParser()
		parser.parse(ArrayBuffer("book","that","flight"),loadTestGrammar, EarleyParser.isNextCatPOS, EarleyParser.scanner, EarleyParser.deterministicPredictor, EarleyParser.deterministicCompleter,EarleyParser.defaultTreeSelector)
		// loadTestGrammar
	}

	def loadTestGrammar():Grammar={
      var testGrammar=new Grammar()
      // var s=Var("START")

      // System.out.println(s.equals(Var("START")))

      // var set=HashSet[GrammarElement]()
      // set+=Var("START")
      // System.out.println(set.contains(Var("START")))
      testGrammar+=Tuple2(Var("START"),ElemListArray())
      testGrammar+=Tuple2(Var("NP"),ElemListArray())
      testGrammar+=Tuple2(Var("Nominal"),ElemListArray())
      testGrammar+=Tuple2(Var("VP"),ElemListArray())
      testGrammar+=Tuple2(Var("Det"),ElemListArray())
      testGrammar+=Tuple2(Var("Noun"),ElemListArray())
      testGrammar+=Tuple2(Var("Verb"),ElemListArray())
      testGrammar+=Tuple2(Var("Aux"),ElemListArray())
      testGrammar+=Tuple2(Var("Prep"),ElemListArray())
      testGrammar+=Tuple2(Var("Proper-Noun"),ElemListArray())      

      testGrammar(Var("START"))+=ElemList(Var("NP"),Var("VP"))
      testGrammar(Var("START"))+=ElemList(Var("Aux"),Var("NP"),Var("VP"))
      testGrammar(Var("START"))+=ElemList(Var("VP"))

      testGrammar(Var("NP"))+=ElemList(Var("Det"),Var("Nominal"))
      testGrammar(Var("NP"))+=ElemList(Var("Proper-Noun"))

      testGrammar(Var("Nominal"))+=ElemList(Var("Noun"))
      testGrammar(Var("Nominal"))+=ElemList(Var("Noun"),Var("Nominal"))
      // testGrammar(Var("Nominal"))+=ElemList(Var("Nominal"),Var("PP"))

      testGrammar(Var("VP"))+=ElemList(Var("Verb"))
      testGrammar(Var("VP"))+=ElemList(Var("Verb"),Var("NP"))

      testGrammar(Var("Det"))+=ElemList(Term("that"))
      testGrammar(Var("Det"))+=ElemList(Term("this"))
      testGrammar(Var("Det"))+=ElemList(Term("a"))

      testGrammar(Var("Noun"))+=ElemList(Term("book"))
      testGrammar(Var("Noun"))+=ElemList(Term("flight"))
      testGrammar(Var("Noun"))+=ElemList(Term("meal"))
      testGrammar(Var("Noun"))+=ElemList(Term("money"))

      testGrammar(Var("Verb"))+=ElemList(Term("book"))
      testGrammar(Var("Verb"))+=ElemList(Term("include"))
      testGrammar(Var("Verb"))+=ElemList(Term("prefer"))

      testGrammar(Var("Aux"))+=ElemList(Term("does"))

      testGrammar(Var("Prep"))+=ElemList(Term("from"))
      testGrammar(Var("Prep"))+=ElemList(Term("to"))
      testGrammar(Var("Prep"))+=ElemList(Term("on"))

      testGrammar(Var("Proper-Noun"))+=ElemList(Term("Houston"))
      testGrammar(Var("Proper-Noun"))+=ElemList(Term("TWA"))

      var invList=HashMap[String,HashSet[GrammarElement]]()
      invList+=Tuple2("that",HashSet[GrammarElement](Var("Det")))
      invList+=Tuple2("this",HashSet[GrammarElement](Var("Det")))
      invList+=Tuple2("a",HashSet[GrammarElement](Var("Det")))
      invList+=Tuple2("book",HashSet[GrammarElement](Var("Noun"),Var("Verb")))
      invList+=Tuple2("flight",HashSet[GrammarElement](Var("Noun")))
      invList+=Tuple2("meal",HashSet[GrammarElement](Var("Noun")))
      invList+=Tuple2("money",HashSet[GrammarElement](Var("Noun")))
      invList+=Tuple2("include",HashSet[GrammarElement](Var("Verb")))
      invList+=Tuple2("prefer",HashSet[GrammarElement](Var("Verb")))
      invList+=Tuple2("does",HashSet[GrammarElement](Var("Aux")))
      invList+=Tuple2("from",HashSet[GrammarElement](Var("Prep")))
      invList+=Tuple2("to",HashSet[GrammarElement](Var("Prep")))
      invList+=Tuple2("on",HashSet[GrammarElement](Var("Prep")))
      invList+=Tuple2("Houston",HashSet[GrammarElement](Var("Proper-Noun")))
      invList+=Tuple2("TWA",HashSet[GrammarElement](Var("Proper-Noun")))

      testGrammar.wordCatInvList=Some(invList)
      System.out.println(testGrammar)
      testGrammar
	}

	def runPenn(){
		var parser=new EarleyParser()
		// var words=ArrayBuffer("(","IP","(","NP","(","NPR","Senhor",")",")","(","PERIOD",":",")","(","VB","Ofereço",")","(","PP","(","P","a",")",
		// 	"(","NP","(","PRO$","Vossa",")","(","NPR","Majestade",
		// 	")",")",")","(","NP","(","D","as",")","(","NPR","Reflexões",")","(","PP","(","P","sobre",")","(","NP","(","D","a",")","(","N","vaidade",")","(","PP","(","P+D","dos",")",
		// 		"(","NP","(","N","homens",")",")",")",")",")",")","(","PERIOD",";",")",")")
		var words1=ArrayBuffer("(","IP","(","NP","(","NPR","Senhor",")",")","(","PERIOD",":",")",")")
		parser.parse(words1,loadPennGrammar, CorpusTraining.isNextCatPOSForPenn, CorpusTraining.scannerPenn, EarleyParser.deterministicPredictor, EarleyParser.deterministicCompleter,EarleyParser.defaultTreeSelector)
	}

	def loadPennGrammar():Grammar={
      var pennGrammar=Grammar()
      pennGrammar+=Tuple2(Var("START"),ElemListArray())      
      pennGrammar+=Tuple2(Var("LIST"),ElemListArray())
      pennGrammar+=Tuple2(Var("TERM"),ElemListArray())
      pennGrammar+=Tuple2(Var("NONTERM"),ElemListArray())
      pennGrammar+=Tuple2(Var("LEFTPAR"),ElemListArray(ElemList(Term("("))))
      pennGrammar+=Tuple2(Var("RIGHTPAR"),ElemListArray(ElemList(Term(")"))))

      pennGrammar(Var("START"))+=ElemList(Var("LEFTPAR"),Var("NONTERM"),Var("LIST"),Var("RIGHTPAR"))
      pennGrammar(Var("LIST"))+=ElemList(Var("LEFTPAR"),Var("NONTERM"),Var("LIST"),Var("RIGHTPAR"),Var("LIST"))
      pennGrammar(Var("LIST"))+=ElemList(Var("LEFTPAR"),Var("NONTERM"),Var("LIST"),Var("RIGHTPAR"))
      pennGrammar(Var("LIST"))+=ElemList(Var("TERM"))

      pennGrammar
    }

    def testSentenceExtractor(){
    	var parser=new EarleyParser()
		var words=ArrayBuffer("(","IP","(","NP","(","NPR","Senhor",")",")","(","PERIOD",":",")","(","VB","Ofereço",")","(","PP","(","P","a",")",
			"(","NP","(","PRO$","Vossa",")","(","NPR","Majestade",
			")",")",")","(","NP","(","D","as",")","(","NPR","Reflexões",")","(","PP","(","P","sobre",")","(","NP","(","D","a",")","(","N","vaidade",")","(","PP","(","P+D","dos",")",
				"(","NP","(","N","homens",")",")",")",")",")",")","(","PERIOD",";",")",")")

		var words1=ArrayBuffer("(","IP","(","NP","(","NPR","Senhor",")",")","(","PERIOD",":",")","(","VB","Ofereço",")",")")

		var words2=ArrayBuffer("(","IP","(","NP","(","NPR","Senhor",")",")","(","PERIOD",":",")",")")
		var trees=parser.parse(words,loadPennGrammar, CorpusTraining.
			isNextCatPOSForPenn, CorpusTraining.scannerPenn, EarleyParser.deterministicPredictor, EarleyParser.deterministicCompleter,EarleyParser.defaultTreeSelector)

		System.out.println("\n"+trees.map( t => t.getSentence))
    }

    def testTreeExtractor(){
    	var parser=new EarleyParser()
		var words=ArrayBuffer("(","IP","(","NP","(","NPR","Senhor",")",")","(","PERIOD",":",")","(","VB","Ofereço",")","(","PP","(","P","a",")",
			"(","NP","(","PRO$","Vossa",")","(","NPR","Majestade",
			")",")",")","(","NP","(","D","as",")","(","NPR","Reflexões",")","(","PP","(","P","sobre",")","(","NP","(","D","a",")","(","N","vaidade",")","(","PP","(","P+D","dos",")",
				"(","NP","(","N","homens",")",")",")",")",")",")","(","PERIOD",";",")",")")

		var words1=ArrayBuffer("(","IP","(","NP","(","NPR","Senhor",")",")","(","PERIOD",":",")","(","VB","Ofereço",")","(","PP","(","P","a",")",")",
			"(","PERIOD",":",")",")")
		var words2=ArrayBuffer("(","IP","(","NP","(","NPR","Senhor",")",")","(","PERIOD",":",")",")")
		var trees=parser.parse(words1,loadPennGrammar, CorpusTraining.isNextCatPOSForPenn, CorpusTraining.scannerPenn, EarleyParser.deterministicPredictor, EarleyParser.deterministicCompleter,EarleyParser.defaultTreeSelector)

		System.out.println("TREES")
		System.out.println("===========================")
		System.out.println(trees)
		System.out.println(trees.map(t => t.getConstituents))
    }

    def testGrammarExtractor():Grammar={
    	var parser=new EarleyParser()
		var words=ArrayBuffer("(","IP","(","NP","(","NPR","Senhor",")",")","(","PERIOD",":",")","(","VB","Ofereço",")","(","PP","(","P","a",")",
			"(","NP","(","PRO$","Vossa",")","(","NPR","Majestade",
			")",")",")","(","NP","(","D","as",")","(","NPR","Reflexões",")","(","PP","(","P","sobre",")","(","NP","(","D","a",")","(","N","vaidade",")","(","PP","(","P+D","dos",")",
				"(","NP","(","N","homens",")",")",")",")",")",")","(","PERIOD",";",")",")")
		var words1=ArrayBuffer("(","IP","(","NP","(","NPR","Senhor",")",")","(","PERIOD",":",")","(","VB","Ofereço",")",")")
		
		var words2=ArrayBuffer("(","IP","(","NP","(","NPR","Senhor",")",")","(","PERIOD",":",")",")")

		var words3=ArrayBuffer("(","IP","(","NP","(","NPR","Senhor",")",")","(","PERIOD",":",")","(","VB","Ofereço",")","(","PP","(","P","a",")",
			"(","NP","(","PRO$","Vossa",")","(","NPR","Majestade",
			")",")",")",")")

		var trees=parser.parse(words,loadPennGrammar, CorpusTraining.isNextCatPOSForPenn, CorpusTraining.scannerPenn, EarleyParser.deterministicPredictor, EarleyParser.deterministicCompleter,EarleyParser.defaultTreeSelector)
		
		var grammar=GrammarExtractor.extract(trees)
		System.out.println("\nGRAMMAR")
		System.out.println("=================")
		System.out.println(grammar)
		
		grammar
    }

    def testProbabilisticGrammar():Grammar={
 		var grammar = Grammar()
	    grammar += Tuple2(Var("START"), ElemListArray())
	    grammar += Tuple2(Var("NP"), ElemListArray())
	    grammar += Tuple2(Var("VP"), ElemListArray())
	    grammar += Tuple2(Var("ART"), ElemListArray())
		grammar += Tuple2(Var("V"), ElemListArray())
		grammar += Tuple2(Var("N"), ElemListArray())
		grammar += Tuple2(Var("ADJ"), ElemListArray())
	    

	    grammar(Var("START")) += ElemList(1.0,Var("NP"), Var("VP"))
	    grammar(Var("NP")) += ElemList(0.7,Var("ART"), Var("N"))	    
	    grammar(Var("VP")) += ElemList(0.4,Var("V"), Var("NP"))
	    grammar(Var("VP")) += ElemList(0.6,Var("V"), Var("ADJ"))
	    grammar(Var("ART")) += ElemList(1.0,Term("the"))
	    grammar(Var("V")) += ElemList(1.0,Term("is"))
	    grammar(Var("N")) += ElemList(0.8,Term("ball"))
	    grammar(Var("N")) += ElemList(0.2,Term("apple"))
	    grammar(Var("ADJ")) += ElemList(0.4,Term("heavy"))
	    grammar(Var("ADJ")) += ElemList(0.6,Term("blue"))

	    var invList=HashMap[String,HashSet[GrammarElement]]()
      invList+=Tuple2("the",HashSet[GrammarElement](Var("ART")))
      invList+=Tuple2("is",HashSet[GrammarElement](Var("V")))
      invList+=Tuple2("ball",HashSet[GrammarElement](Var("N")))
      invList+=Tuple2("apple",HashSet[GrammarElement](Var("N")))
      invList+=Tuple2("heavy",HashSet[GrammarElement](Var("ADJ")))
      invList+=Tuple2("blue",HashSet[GrammarElement](Var("ADJ")))
      

	  grammar.wordCatInvList=Some(invList)

	  var parser = new EarleyParser()
	  var trees=ArrayBuffer[Tree]()
	  var sentence=ArrayBuffer[String]("the","ball","is","heavy")
	  trees ++= parser.parse(sentence, grammar, EarleyParser.isNextCatPOS, EarleyParser.probabilisticScanner, EarleyParser.probabilisticPredictor, EarleyParser.probabilisticCompleter,EarleyParser.maxProbTreeSelector)

	  System.out.println(trees)
	  grammar
    }
}
